\ssection{Comparison of cohomology groups of different coverings}
\setcounter{subsection}{26}

In this paragraph, $X$ denotes a topological space and $\Ff$ is a sheaf on $X$.
We pose conditions on a covering $\Uu$ of $X$, under which we have
$\HH^n(\Uu, \Ff) = \HH^n(X, \Ff)$ for all $n\geq 0$.

\ssubsection{Double complexes}
\label{section-\arabic{subsection}}

A double complex (cf. \cite{6}, Chapter VI, \S 4) is a bigraded
abelian group
\[ K = \bigoplus_{p, q} K^{p, q}, \quad p\geq 0, q\geq 0, \]
equipped with two endomorphisms $d'$ and $d''$ satisfying the following
properties:\\
--- $d'$ maps $K^{p, q}$ to $K^{p+1, q}$ and $d''$ maps $K^{p,q}$ to $K^{p, q+1}$,\\
--- $d'\circ d'=0$, $d'\circ d'' + d''\circ d' = 0$, $d''\circ d''=0$.

An element of $K^{p,q}$ is said to be bihomogenous of bidegree $(p, q)$, and of total
degree $p+q$. The endomorphism $d = d' + d''$ satisfies $d\circ d=0$, and the cohomology
groups of $K$ with respect to this coboundary operator are denoted by $H^n(K)$,
where $n$ means the total degree.

We can treat $d'$ as a coboundary operator on $K$; since $d'$ is compatible with
the bigrading of $K$, we also obtain cohomology groups, denoted by $\HH_{I}^{p,q}(K)$;
for $d''$, we have the groups $\HH_{II}^{p,q}(K)$.

We denote by $K_{II}^q$ the subgroup of $K^{0,q}$  consisting of elements $x$ such
that $d'(x) = 0$, and by $K_{II}$ the direct sum of $K_{II}^q$ ($q=0, 1, \ldots$).
We have an analogous definition of $K_I = \bigoplus_{p=0}^\infty K_{I}^p$. We 
note that
\[ K_{II}^q = \HH_I^{0,q}(K)\quad \text{and} \quad K_I^p = \HH_{II}^{p,0}(K). \]

$K_{II}$ is a subcomplex of $K$, and the operator $d$ coincides on $K_{II}$ with
the operator $d''$.

% page 220

{\bf Proposition 1.} \emph{If $\HH_I^{p,q}(K) = 0$ for $p>0$ and $q\geq 0$,
the inclusion $K_{II}\to K$ defines a bijection from $\HH^n(K_{II})$ to $\HH^n(K)$,
for all $n\geq 0$. }

(Cf. \cite{4}, statement XVII-6, whose proof we shall repeat here).

By replacing $K$ by $K/K_{II}$, we are led to prove that if $\HH_{I}^{p,q}(K)=0$
for $p\geq 0$ and $q\geq 0$, then $H^n(K) = 0$ for all $n\geq 0$. Put
\[ K_h = \bigoplus_{q\geq h} K^{p, q}. \]
The groups $K_h$ ($h=0, 1, \ldots$) are subcomplexes embedded in $K$, and
$K_h/K_{h+1}$ is isomorphic to $\bigoplus_{p=0}^\infty K^{p, h}$, equipped
with the coboundary operator $d'$. We thus have $\HH^n(K_h/K_{h+1})=
\HH_I^{h, n-h}(K) = 0$ for any $n$ and $h$, therefore $\HH^n(K_h) = \HH^n(K_{h+1})$.
Since $\HH^n(K_h) = 0$ if $h > n$, we deduce, by descending recursion on $h$,
that $\HH^n(K_h) = 0$ for all $n$ and $h$, and since $K_0$ is equal to $K$,
the Proposition follows.

\ssubsection{The double complex defined by two coverings}
\label{section-\arabic{subsection}}

Let $\Uu = \{ U_i \}_{i\in I}$ and $\Bb = \{ V_j \}_{j\in J}$ be two coverings of $X$.
If $s$ is a $p$-simplex of $S(I)$ and $s'$ a $q$-simplex of $S(J)$, we denote by
$U_s$ the intersection of $U_i$, $i\in s$ (cf. \no \ref{section-18}), the intersection of $V_j$,
$j\in s'$, by $\Bb_s$ the covering of $U_s$ formed by $\{ U_s\cap V_j\}_{j\in J}$
and by $\Uu_{s'}$ the covering of $V_{s'}$ formed by $\{ V_{s'}\cap U_i\}_{i\in I}$.

We define a double complex $C(\Uu, \Bb; \Ff) = \bigoplus_{p, q} C^{p, q}(\Uu, \Bb; \Ff)$
as follows:

$C^{p,q}(\Uu,\Bb;\Ff) = \prod\Gamma(U_s\cap V_{s'}, \Ff)$, the product taken over
all pairs $(s, s')$ where $s$ is a simplex of dimension $p$ of $S(I)$ and $s'$ is
a simplex of dimension $q$ of $S(J)$.

An element $f\in C^{p,q}(\Uu, \Bb; \Ff)$ is thus a system $(f_{s, s'})$ of sections
of $\Ff$ on $U_{s}\cap V_{s'}$ or, with the notations of \no \ref{section-18}, it is a system
\[ f_{i_0\ldots i_p, j_0\ldots j_q} \in \Gamma(U_{i_0\ldots i_p}\cap V_{j_0\ldots j_q}, 
\Ff). \]

We can also identify $C^{p,q}(\Uu, \Bb; \Ff)$ with $\prod_{s'} C^p(\Uu_{s'}, \Ff)$;
thus, for all $s'$, we have a coboundary operator $d: C^p(\Uu_{s'}, \Ff
\to C^{p+1}(\Uu_{s'}, \Ff)$, giving a homomorphism
\[ d_{\Uu} : C^{p, q}(\Uu, \Bb; \Ff) \to C^{p+1, q}(\Uu, \Bb; \Ff). \]

Making the definition of $d_{\Uu}$ explicit, we obtain:
\[ (d_\Uu f)_{i_0\ldots i_{p+1}, j_0\ldots j_q} 
    = \sum_{k=0}^{k=p+1} (-1)^k \rho_k(f_{i_0\ldots \hat{i}_k \ldots i_{p+1}, 
j_0\ldots j_q}),\]
$\rho_k$ being the restriction homomorphism defined by the inclusion of
\[ U_{i_0\ldots i_p}  \cap V_{j_0\ldots j_q}\quad\text{in}\quad U_{i_0\ldots i_k
\ldots i_{p+1}}\cap V_{j_0\ldots j_q} . \]

We define $d_\Bb: C^{p,q}(\Uu,\Bb;\Ff)\to C^{p, q+1}(\Uu,\Bb;\Ff)$ the same way
and we have
\[ (d_\Bb f)_{i_0\ldots i_p, j_0\ldots j_{q+1}} 
    = \sum_{h=0}^{h=q+1} (-1)^h \rho_h(f_{i_0\ldots i_p, j_0\ldots\hat{j}_h\ldots
 j_{q+1}}).\]

It is clear that $d_\Uu\circ d_\Uu = 0$, $d_\Uu\circ d\Bb= d_\Bb\circ d_\Uu$,
$d_\Bb\circ d_\Bb = 0$. We thus put 
%
% page 221 
%
$d' = d_\Uu$, $d'' = (-1)^p d_\Bb$, equipping
$C(\Uu, \Bb; \Ff)$ with a structure of a double complex. We now apply
to $K = C(\Uu, \Bb; \Ff)$ the definitions from the preceding \no; the groups
or complexes denoted in the general case by $\HH^n(K)$, $\HH_I^{p, q}(K)$,
$\HH_{I}^{p,q}(K)$, $\HH_{II}^{p,q}(K)$, $K_I$, $K_{II}$ will be denoted
by $\HH^n(\Uu,\Bb;\Ff)$, $\HH_I^{p,q}(\Uu,\Bb;\Ff)$, $\HH_{II}^{p,q}(\Uu,\Bb;\Ff)$,
$C_I(\Uu,\Bb;\Ff)$ and $C_{II}(\Uu,\Bb;\Ff)$, respectively.

From the definitions of $d'$ and $d''$, we immediately obtain:

{\bf Proposition 2.} \emph{$\HH_{I}^{p,q}(\Uu,\Bb;\Ff)$ is isomorphic to
$\prod_{s'} \HH^p(\Uu_{s'}, \Ff)$, the product being taken over all simplexes
of dimension $q$ of $S(J)$. In particular, }
\[ C_{II}^q(\Uu,\Bb;\Ff) = \HH_I^{0, q}(\Uu,\Bb;\Ff) \]
\emph{is isomorphic to $\prod_{s'} \HH^0(\Uu_{s'}, \Ff) = C^q(\Bb, \Ff)$.}

We denote by $\iota''$ the canonical isomorphism: $C(\Bb, \Ff)\to C_{II}(\Uu,\Bb;\Ff)$.
If $(f_{j_0\ldots j_q})$ is an element of $C^q(\Bb, \Ff)$, we thus have
\[ (\iota'' f)_{i_0, j_0\ldots j_q} = \rho_{i_0}(f_{j_0\ldots j_q}), \]
where $\rho_{i_0}$ denotes the restriction homomorphism defined by the inclusion of
\[ U_{i_0}\cap V_{j_0\ldots j_q}\quad\text{in}\quad V_{j_0\ldots j_q}. \]

Obviously, a statement analogous to Proposition 2 holds for $\HH^{p,q}_II(\Uu,\Bb;\Ff)$,
and we have an isomorphism $\iota': C(\Uu,\Ff)\to C_I(\Uu,\Bb;\Ff)$.

\ssubsection{Applications}
\label{section-\arabic{subsection}}

{\bf Proposition 3.} \emph{Assume that $\HH^p(\Uu_{s'}, \Ff) = 0$ for every $s'$
and all $p>0$. Then the homomorphism $\HH^n(\Bb, \Ff)\to\HH^n(\Uu,\Bb;\Ff)$, defined
by $\iota''$, is bijective for all $n\geq 0$. }

This is an immediate consequence of Propositions 1 and 2.

Before stating Proposition 4, we prove a lemma:

{\bf Lemma 1.} \emph{Let $\mathfrak{W} = \{ W_i\}_{i\in I}$ be a covering of a space
$Y$ and let $\Ff$ be a sheaf on $Y$. If there exists an $i\in I$ such that
$W_i = Y$, then $\HH^p(\mathfrak{W}, \Ff) = 0$ for all $p>0$. }

Let $\mathfrak{W}'$ be a covering of $Y$ consisting of a single open set $Y$;
we obviously have $\mathfrak{W}\prec \mathfrak{W}'$, and the assumption made 
on $\mathfrak{W}$ means that $\mathfrak{W}'\prec \mathfrak{W}$. In result
(\no \ref{section-22}) we have $\HH^p(\mathfrak{W}, \Ff) = \HH^p(\mathfrak{W}', \Ff) = 0$
if $p>0$.

{\bf Proposition 4.} \emph{Suppose that the covering $\Bb$ is finer than
the covering $\Uu$. Then $\iota'' : \HH^n(\Bb, \Ff)\to\HH^n(\Uu,\Bb;\Ff)$ is
bijective for all $n\geq 0$. Moreover, the homomorphism $\iota'\circ \iota''^{-1}
: \HH^n(\Uu, \Ff) \to \HH^n(\Bb, \Ff)$ coincides with the homomorphism
$\sigma(\Bb, \Uu)$ defined in \no \ref{section-21}. }

We apply Lemma 1 to $\mathfrak{W}=\Uu_{s'}$ and $Y = V_{s'}$, seeing that
$\HH^p(\Uu_{s'}, \Ff) = 0$ for all $p>0$, and then Proposition 3 shows that
\[ \iota'': \HH^n(\Bb, \Ff) \to \HH^n(\Uu,\Bb;\Ff) \]
is bijective for all $n\geq 0$.

Let $\tau: J\to I$ be a mapping such that $V_j\subset U_{\tau j}$; for the
proof of the second part of the Proposition, we need to observe that
if $f$ is an $n$-cocycle of $C(\Uu, \Ff)$, the cocycles $\iota'(f)$
and $\iota''(\tau f)$ are cohomologous in $C(\Uu, \Bb; \Ff)$.

% page 222

For any integer $p$, $0\leq p\leq n-1$, define $g^p\in C^{p, n-p-1}(\Uu,\Bb;\Ff)$
by the following formula
\[ g^p_{i_0\ldots i_p, j_0\ldots j_{n-p-1}} = \rho_p(f_{i_0\ldots i_p
   \tau j_0\ldots \tau j_{n-p}}), \]
$\rho_p$ denoting the restriction defined by the inclusion of 
\[ U_{i_0\ldots i_p}\cap V_{j_0\ldots j_{n-p-1}} \quad\text{in}\quad
	U_{i_0\ldots i_p, \tau j_0\ldots \tau j_{n-p-1}}. \]

We verify by a direct calculation (keeping in mind that $f$ is a cocycle)
that we have
\[ d''(g^0) = \iota''(\tau f), \ldots, d''(g^p) = d'(g^{p-1}), \ldots
    d'(g^{n-1}) = (-1)^n \iota'(f) \]
hence $d(g^0 - g^1 + \ldots + (-1)^{n-1}g^{n-1}) = \iota''(\tau f) - \iota'(f)$,
which shows that $\iota''(\tau f)$ and $\iota'(f)$ are cohomologous.

{\bf Proposition 5.} \emph{Suppose that $\Bb$ is finer than $\Uu$ and that
$\HH^q(\Bb_s, \Ff) = 0$ for all $s$ and all $q>0$. Then the homomorphism
$\sigma(\Bb, \Uu): \HH^n(\Uu, \Ff)\to\HH^n(\Bb, \Ff)$ is bijective for
all $n\geq 0$. }

If we apply Proposition 3, switching the roles of $\Uu$ and $\Bb$, we
see that $\iota':\HH^n(\Bb, \Ff)\to\HH^n(\Uu,\Bb;\Ff)$ is bijective. The
Proposition then follows directly from Proposition 4. 

{\bf Theorem 1.} \emph{Let $X$ be a topological space, $\Uu=\{U_i\}_{i\in I}$ a
covering of $X$, $\Ff$ a sheaf on $X$. Assume that there exists a family
$\Bb^\alpha$, $\alpha\in A$ of coverings of $X$ satisfying the following
properties:\\
\wciecie {\bf (a)} For any covering $\mathfrak{W}$ of $X$, there exists 
an $\alpha\in A$ with $\Bb^\alpha\prec\mathfrak{W}$,\\
\wciecie {\bf (b)} $\Hq(\Bb_s^\alpha, \Ff) = 0$ for all $\alpha\in A$,
all simplexes $s\in S(I)$ and every $q>0$,\\
Then $\sigma(\Uu): \HH^n(\Uu, \Ff)\to\HH^n(X, \Ff)$ is bijective for all $n\geq 0$. }

Since $\Bb^\alpha$ are arbitrarily fine, we can assume that they are finer than
$\Uu$. In this case, the homomorphism
\[ \sigma(\Bb^\alpha, \Uu) : \HH^n(\Uu, \Ff) \to \HH^n(\Bb^\alpha, \Ff) \]
is bijective for all $n\geq 0$, by Proposition 5. Because $\Bb^\alpha$ are
arbitrarily fine, $\HH^n(X, \Ff)$ is the inductive limit of $\HH^n(\Bb^\alpha, \Ff)$,
and the theorem follows.

{\bf Remarks.} {\bf (1)} It is probable that Theorem 1 remains valid when
we replace the condition (b) with the following weaker condition:\\
\wciecie (b') $\lim_\alpha \HH^q(\Bb^\alpha_s, \Ff) = 0$ for any simplex $s$
of $S(I)$ and any $q>0$. 

{\bf (2)} Theorem 1 is analogous to a theorem of Leray on acyclic coverings.
Cf. \cite{10} and also \cite{4}, statement XVII-7.

