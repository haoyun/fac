% page 243

\ssection{Projective varieties}
\setcounter{subsection}{50}

\ssubsection{Notations}
\label{section-\arabic{subsection}}

(The notations introduced below will be used without reference during
the whole chapter).

Let $r$ be an integer $\geq 0$ and let $Y = K^{r+1} - \{0\}$; the multiplicative
group $K^*$ of nonzero elements of $K$ acts on $Y$ by the formula
\[ \lambda(\mu_0, \ldots, \mu_r) = (\lambda\mu_0, \ldots, \lambda \mu_r). \]

Two points $y$ and $y'$ will be called equivalent if there exists $\lambda\in K^*$
such that $y' = \lambda y$; the quotient space of $Y$ by this equivalence relation
will be denoted by $\Pp_r(K)$ or simply $X$; it is the \emph{projective space of
dimension $r$ over $K$}; the canonical projection of $Y$ onto $X$ will be denoted $\pi$.

Let $I= \{0, 1, \ldots, r\}$; for every $i\in I$, we denote by $t_i$ the $i$-th
coordinate function on $K^{r+1}$, defined by the formula:
\[ t_i(\mu_0, \ldots, \mu_r) = \mu_i .\]

We denote by $V_i$ the open subset of $K^{r+1}$ consisting of points whose $t_i$
is $\neq 0$ and by $U_i$ the image of $V_i$ by $\pi$; the $\{U_i\}$ form a covering
$\Uu$ of $X$. If $i\in I$ and $j\in I$, the function $t_j/t_i$ is regular on $V_i$
and invariant for $K^*$, thus defines a function on $U_i$ which we denote also
by $t_j/t_i$; for fixed $i$, the functions $t_j/t_i$, $j\neq i$ define a bijection
$\phi_i : U_i \to K^r$.

We equip $K^{r+1}$ with the structure of an algebraic variety and $Y$ the induced
structure. Likewise, we equip $X$ with the quotient topology from $Y$: a closed
subset of $X$ is thus the image by $\pi$ of a closed cone in $K^{r+1}$. 
If $U$ is open in $X$, we define $A_U = \Gamma(\pi^{-1}(U), \Oo_Y)$; this is
the sheaf of regular functions on $\pi^{-1}(U)$. Let $A^0_U$ be the subring of $A_U$
consisting of elements invariant for $K^*$ (that is, homogeneous functions of degree $0$).
When $V\supset U$, we have a restriction homomorphism $\phi_U^V: A^0_V\to A^0_U$
and the system $(A_U^0, \phi_U^V)$ defines a sheaf $\Oo_X$ which can be considered as
a subsheaf of the sheaf $\Ff(X)$ of germs of functions on $X$. Such a function $f$,
defined in a neighborhood of $x$ belongs to $\Oo_{x, X}$ if and only if it coincides
locally with a function of the form $P/Q$ where $P$ and $Q$ are homogeneous polynomials 
%
% page 244
%
of the same degree in $t_0, \ldots, t_r$ with $Q(y)\neq 0$ for $y\in \pi^{-1}(x)$
(which we write for brevity as $Q(x)\neq 0$).

{\bf Proposition 1.} \emph{The projective space $X = \Pp_r(K)$, equipped with the topology
and sheaf above, is an algebraic variety.}

The $U_i$, $i\in I$ are open in $X$ and we verify immediately that the bijections
$\phi_i: U_i\to K^r$ defined above are biregular isomorphisms, which shows that the
axiom ($VA_I$) is satisfied. To show that ($VA_{II}$) is also satisfied, we must
observe that the subset of $K^r\times K^r$ consisting of all pairs $(\psi_i(x),\psi_j(x))$
where $x\in U_i\cap U_j$ is closed, which does not pose difficulties.

In what follows, $X$ will be always equipped with the structure of an algebraic variety
just defined; the sheaf $\Oo_X$ will be simply denoted $\Oo$. An algebraic variety
$V$ is called \emph{projective} if it is isomorphic to a closed subvariety of
a projective space. The study of coherent algebraic sheaves on projective varieties
can be reduced to the study of coherent algebraic sheaves on $\Pp_r(K)$, cf. \no \ref{section-39}.

\ssubsection{Cohomology of subvarieties of the projective space}
\label{section-\arabic{subsection}}

Let us apply Theorem 4 from \no \ref{section-47} to the covering $\Uu = \{U_i\}_{i\in I}$ defined
in the preceding \no: it is possible since each $U_i$ is isomorphic to $K^r$.
We thus obtain:

{\bf Proposition 2.} \emph{If $\Ff$ is a coherent algebraic sheaf on $X=\Pp_r(K)$,
the homomorphism $\sigma(\Uu): \HH^n(\Uu, \Ff)\to \HH^n(X, \Ff)$ is bijective
for all $n\neq 0$.}

Since $\Uu$ consists of $r+1$ open subsets, we have (cf. \no \ref{section-20}, corollary to 
Proposition 2):

{\bf Corollary.} \emph{$\HH^n(X, \Ff) = 0$ for $n>r$.}

This result can be generalized in the following way:

{\bf Proposition 3.} \emph{Let $V$ be an algebraic variety, isomorphic to a 
locally closed subvariety of the projective space $X$. Let $\Ff$ be an algebraic
coherent sheaf on $V$ and let $W$ be the subvariety of $V$ such that $\Ff$
is zero outside $W$. We then have $\HH^n(V, \Ff) = 0$ for $n>\dim W$.}

In particular, taking $W=V$, we see that we have:

{\bf Corollary.} \emph{$\HH^n(V, \Ff) = 0$ for $n>\dim V$.}

Identify $V$ with a locally closed subvariety of $X = \Pp_r(K)$; there exists
an open subset $U$ of $X$ such that $V$ is closed in $U$. We can clearly assume
that $W$ is closed in $V$, so that $W$ is closed in $U$. Let $F = X - U$.
Before proving Proposition 3, we establish two lemmas:

{\bf Lemma 1.} \emph{Let $k=\dim W$; there exists $k+1$ homogeneous polynomials 
$P_i(t_0, \ldots, t_r)$ of degrees $>0$, vanishing on $F$ and not vanishing 
simultaneously on $W$.}

(By abuse of language, we say that a homogeneous polynomial $P$ vanishes in
a point $x$ of $\Pp_r(K)$ if it vanishes on $\pi^{-1}(x)$).

We proceed by induction on $k$, the case when $k=-1$ being trivial. Choose
a point on each irreducible component of $W$ and let $P_1$ be a homogeneous
polynomial vanishing on $F$, of degree $>0$ and nonvanishing in each of these
points (the existence of $P_1$ follows from the fact that $F$ is closed, given
the definition of the topology of $\Pp_r(K)$). Let $W'$ be a subvariety of $W$
consisting of points $x\in W$ such that 
%
% page 245
%
$P_1(x) = 0$; by the construction of $P_1$, no irreducible component of $W$ is
contained in $W'$ and it follows (cf. \no \ref{section-36}) that $\dim W'<k$. Applying the
induction assumption to $W'$, we see that there exist $k$ homogeneous polynomials
$P_2, \ldots, P_{k+1}$ vanishing on $F$ and nonvanishing simultaneously on $W'$;
it is clear that the polynomials $P_1, \ldots, P_{k+1}$ satisfy appropriate conditions.

{\bf Lemma 2.} \emph{Let $P(t_0, \ldots, t_r)$ be a homogeneous polynomial of 
degree $n>0$. The set $X_P$ of all points $x\in X$ such that $P(x)\neq 0$
is an open affine subset of $X$.}

If we assign to every point $y = (\mu_0, \ldots, \mu_r)\in Y$ the point of 
the space $K^N$ having for coordinates all monomials $\mu_0^{m_0}\ldots
\mu_r^{m_r}$, $m_0 + \ldots + m_r = n$, we obtain, by passing to quotient,
a mapping $\phi_n: X\to \Pp_{N-1}(K)$. It is classical, and also easy to 
verify, that $\phi_n$ is a biregular isomorphism of $X$ onto a closed subvariety
of $\Pp^{N-1}(K)$ (,,Veronese variety''); now $\phi_n$ transforms the open 
subset $X_P$ onto the locus of points of $\phi_n(X)$ not lying on a certain
hyperplane of $\Pp_{N-1}(X)$; as the complement of any hyperplane is isomorphic
to an affine space, we conclude that $X_P$ is isomorphic to a closed subvariety
of an affine space.

We shall now prove Proposition 3. Extend the sheaf $\Ff$ by 0 on $U-V$; we
obtain a coherent algebraic sheaf on $U$ which we also denote by $\Ff$,
and we know (cf. \no \ref{section-26}) that $\HH^n(U, \Ff) = \HH^n(V, \Ff)$. Let on the
other hand $P_1, \ldots, P_{k+1}$ be homogeneous polynomials satisfying 
the conditions of Lemma 1; let $P_{k+2}, \ldots, P_h$ be homogeneous polynomials
of degrees $>0$, vanishing on $W\cup F$ and not vanishing simultaneously in 
any point of $U - W$ (to obtain such polynomials, it suffices to take a system
of homogeneous coordinates of the ideal defined by $W\cup F$ in $K[t_0, \ldots,
t_r]$). For every $i$, $1\leq i\leq h$, let $V_i$ be the set of points $x\in X$
such that $P_i(x)\neq 0$; we have $V_i\subset U$ and the assumptions made
above show that $\Bb=\{V_i\}$ is an open covering of $U$; moreover, Lemma 2
shows that $V_i$ are open affine subsets, so $\HH^n(\Bb, \Ff) = \HH^n(U, \Ff)
= \HH^n(V, \Ff)$ for all $n\geq 0$. On the other hand, if $n>k$ and if
the indices $i_0, \ldots, i_n$ are distinct, one of the indices is $>k+1$
and $V_{i_0\ldots i_n}$ does not meet $W$; we conclude that the group of
alternating cochains $C'^n(\Bb, \Ff)$ is zero if $n>k$, which shows that
$\HH^n(\Bb, \Ff) = 0$, by Proposition 2 of \no \ref{section-20}.

\ssubsection{Cohomology of irreducible algebraic curves}
\label{section-\arabic{subsection}}

If $V$ is an irreducible algebraic variety of dimension 1, the closed subsets
of $V$ distinct from $V$ are \emph{finite} subsets. If $F$ is a finite subset
of $V$ and $x$ a point of $F$, we set $V_x^p = (V-F)\cup \{x\}$; the
$V_x^F$, $x\in F$ form a finite open covering $\Bb^F$ of $V$.

{\bf Lemma 3.} \emph{The coverings $\Bb^F$ of the above type are arbitrarily fine.}

Let $\Uu = \{U_i\}_{i\in I}$ be an open covering of $V$, which we may assume to be
finite since $V$ is quasi-compact. We can likewise assume that $U_i\neq \emptyset$
for all $i\in I$. If we set $F_i = V - U_i$, $F_i$ is also finite, and so is
$F = \bigcup_{i\in I} F_i$. We will show that $\Bb^F\prec \Uu$, which proves the
lemma. Let $x\in F$; there exists an $i\in I$ such that $x\notin F_i$, since
the $U_i$ cover $V$; we have then $F-\{x\}\supset F_i$, because $F\supset F_i$,
which means that $V_x^F\subset U_i$ and shows that $\Bb^F\prec\Uu$.

% page 246

{\bf Lemma 4.} \emph{Let $\Ff$ be a sheaf on $V$ and $F$ a finite subset of $V$.
We have
\[ H^n(\Bb^F, \Ff) = 0\]
for $n\geq 2$.}

Set $W = V - F$; it is clear that $V^F_{x_0}\cap\ldots\cap V^F_{x_n} = W$
if $x_0, \ldots, x_n$ are distinct and if $n\geq 1$. If we put $G = \Gamma(W, \Ff)$,
it follows that the alternating complex $C'(\Bb^F, \Ff)$ is isomorphic, in
dimensions $\geq 1$, to $C'(S(F), G)$, $S(F)$ denoting the simplex with $F$
for the set of vertices. It follows that
\[ \HH^n(\Bb^F, \Ff) = \HH^n(S(F), G) = 0 \text{ for } n \geq 2, \]
the cohomology of a simplex being trivial.

Lemmas 3 and 4 obviously imply:

{\bf Proposition 4.} \emph{If $V$ is an irreducible algebraic curve and $\Ff$
is an arbitrary sheaf in $V$, we have $\HH^n(V, \Ff) = 0$ for $n\geq 2$.}

{\bf Remark.} I do not know whether an analogous result is true for varieties
of arbitrary dimension.






