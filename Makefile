pdf:
	pdflatex fac.tex
	pdflatex fac.tex

clean:
	rm -f *.aux *.toc *.log *.out *~ *.bak fac.pdf 
