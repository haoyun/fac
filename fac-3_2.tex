% page 246

\ssection{Graded modules and coherent algebraic sheaves on the projective space}
\setcounter{subsection}{53}

\ssubsection{The operation $\Ff(n)$}
\label{section-\arabic{subsection}}

Let $\Ff$ be an algebraic sheaf on $X = \Pp_r(K)$. Let $\Ff_i = \Ff(U_i)$
be the restriction of $\Ff$ to $U_i$ (cf. \no \ref{section-51}); if $n$ is an arbitrary
integer, let $\theta_{ij}(n)$ be the isomorphism of $\Ff_j(U_i\cap U_j)$
with $\Ff_i(U_i\cap U_j)$ defined by multiplication by the function
$t_j^n/t_i^n$; this makes sense, since $t_j/t_i$ is a regular function
on $U_i\cap U_j$ with values in $K^*$. We have $\theta_{ij}(n)\circ
\theta_{jk}(n) = \theta_{ik}(n)$ at every point of $U_i\cap U_j\cap U_k$;
we can thus apply Proposition 4 of \no \ref{section-4} and obtain an algebraic sheaf
denoted by $\Ff(n)$, defined by gluing the sheaves $\Ff_i = \Ff(U_i)$
using the isomorphisms $\theta_{ij}(n)$.

We have the canonical isomorphisms: $\Ff(0)\isom \Ff$, $\Ff(n)(m)\isom
\Ff(n+m)$. Moreover, $\Ff(n)$ is locally isomorphic to $\Ff$, thus coherent
if $\Ff$ is; it also follows that every exact sequence $\Ff\to\Ff'\to\Ff''$
of algebraic sheaves gives birth to exact sequences $\Ff(n)\to\Ff'(n)\to
\Ff''(n)$ for all $n\in\Zz$.

We can apply the above procedure to the sheaf $\Ff=\Oo$ and so obtain the
sheaves $\Oo(n)$, $n\in\Zz$. We will give another description of these sheaves:
if $U$ is open in $X$, let $A_U^n$ be the subset of $A_U = \Gamma(\pi^{-1}(U),
\Oo_Y)$ consisting of regular functions of degree $n$ (that is, satisfying
the identity $f(\lambda y) = \lambda^n f(y)$ for $\lambda\in K^*$ and
$y\in\pi^{-1}(U)$); the $A^n_U$ are $A_U^0$--modules, thus give birth to
an algebraic sheaf, which we denote by $\Oo'(n)$. An element of $\Oo'(n)_x$,
$x\in X$ can be this identified with a rational function $P/Q$, $P$ and $Q$
being homogeneous polynomials such that $Q(x) \neq 0$ and $\deg P - \deg Q = n$.

{\bf Proposition 1.} \emph{The sheaves $\Oo(n)$ and $\Oo'(n)$ are canonically
isomorphic.}

By definition, a section of $\Oo(n)$ over an open $U\subset X$ is a system
$(f_i)$ of sections of $\Oo$ over $U\cap U_i$ with $f_i = (t_j^n/t_i^n)\cdot f_j$
on $U\cap U_i\cap U_j$; the $f_j$ can be identified with regular functions,
homogeneous of degree 0 over $\pi^{-1}(U)\cap\pi^{-1}(U_i)$; set $g_i=t_i^n\cdot
f_i$; we then have $g_i = g_j$ at every point of $\pi^{-1}(U)\cap\pi^{-1}(U_i)
\cap\pi^{-1}(U_j)$, thus the $g_i$ are the restrictions of a unique regular
%
% page 247
%
function on $\pi^{-1}(U)$, homogeneous of degree $n$. Conversely, such a function
$g$ defines a system $(f_i)$ by setting $f_i = g/t_i^n$. The mapping $(f_i)
\mapsto g$ is thus an isomorphism of $\Oo(n)$ with $\Oo'(n)$.

Henceforth, we will often identify $\Oo(n)$ with $\Oo'(n)$ by means of the
above isomorphism. We observe that a section of $\Oo'(n)$ over $X$ is just
a regular function on $Y$, homogeneous of degree $n$. If we assume that
$r\geq 1$, such a function is identically zero for $n<0$ and it is a
homogeneous \emph{polynomial} of degree $n$ for $n\geq 0$.

{\bf Proposition 2.} \emph{For every algebraic sheaf $\Ff$, the sheaves $\Ff(n)$
and $\Ff\otimes_\Oo\Oo(n)$ are canonically isomorphic.}

Since $\Oo(n)$ is obtained from the $\Oo_i$ by gluing with respect to 
$\theta_{ij}(n)$, $\Ff\otimes\Oo(n)$ is obtained from $\Ff_i\otimes\Oo_i$
by gluing with respect to the isomorphisms $1\otimes\theta_{ij}(n)$;
identifying $\Ff_i\otimes\Oo_i$ with $\Ff_i$ we recover the definition
of $\Ff(n)$.

Henceforth, we will also identify $\Ff(n)$ with $\Ff\otimes\Oo(n)$.

\ssubsection{Sections of $\Ff(n)$}
\label{section-\arabic{subsection}}

Let us first show a lemma on algebraic varieties, that is quite analogous
to Lemma 1 of \no \ref{section-45}:

{\bf Lemma 1.} \emph{Let $V$ be an affine variety, $Q$ a regular function on
$V$ and $V_Q$ the set of all points $x\in V$ such that $Q(x)\neq 0$. Let
$\Ff$ be a coherent algebraic sheaf on $V$ and let $s$ be a section of $\Ff$
over $V_Q$. Then, for $n$ sufficiently large, there exists a section $s'$
of $\Ff$ over the whole $V$ such that $s' = Q^n s$ over $V_Q$.}

Embedding $V$ in an affine space and extending $\Ff$ by 0 outside $V$, 
we are brought to the case where $V$ is an affine space, thus is irreducible.
By Corollary 1 to Theorem 2 from \no \ref{section-45}, there exists a surjective homomorphism
$\phi : \Oo_V^p\to \Ff$; by Proposition 2 of \no \ref{section-42}, $V_Q$ is an open affine
subset and thus there exists (\no \ref{section-44}, Corollary 2 to Proposition 7) a section
$\sigma$ of $\Oo_V^p$ over $V_Q$ such that $\phi(\sigma)=s$. We can
identify $\sigma$ with a system of $p$ regular functions on $V_Q$; 
applying Proposition 5 of \no \ref{section-43} to each of these functions, we see that
there exists a section $\sigma'$ of $\Oo_V^p$ over $V$ such that $\sigma'
=Q^n \sigma$ on $V_Q$, provided that $n$ is sufficiently large. Setting
$s' = \phi(\sigma')$, we obtain a section of $\Ff$ over $V$ such that
$s' = Q^n s$ on $V_Q$.

{\bf Theorem 1.} \emph{Let $\Ff$ be a coherent algebraic sheaf on 
$X = \Pp_r(K)$. There exists an integer $n(\Ff)$ such that for all
$n\geq n(\Ff)$ and all $x\in X$, the $\Oo_x$--module $\Ff(n)_x$
is generated by elements of $\Gamma(X, \Ff(n))$.}

By the definition of $\Ff(n)$, a section $s$ of $\Ff(n)$ over $X$ is
a system $(s_i)$ of sections of $\Ff$ over $U_i$ satisfying the compatibility
conditions:
\[ s_i = (t_j^n/t_i^n)\cdot s_j \text{ on } U_i\cap U_j; \]
we say that $s_i$ is the \emph{$i$-th component of $s$}.

On the other hand, since $U_i$ is isomorphic to $K^r$, there exists a finite
number of sections $s_i^\alpha$ of $\Ff$ over $U_i$ which generate $\Ff_x$
for all $x\in U_i$ (\no \ref{section-45}, Corollary 1 to Theorem 2); if for a certain
integer $n$ we can find sections $s^\alpha$ of
%
% page 248
%
$\Ff(n)$ whose $i$-th component is $s_i^\alpha$, it is clear that 
$\Gamma(X, \Ff(n))$ generates $\Ff(n)_x$ for all $x\in U_i$. Theorem 1 is
thus proven if we prove the following Lemma:

{\bf Lemma 2.} \emph{Let $s_i$ be a section of $\Ff$ over $U_i$. For all
$n$ sufficiently large, there exists a section $s$ of $\Ff(n)$ whose
$i$-th component is equal to $s_i$.}

Apply Lemma 1 to the affine variety $V = U_j$, the function $Q = t_i/t_j$
and the section $s_i$ restricted to $U_i\cap U_j$; this is legal, because
$t_i/t_j$ is a regular function on $U_j$ whose zero set is equal to
$U_j - U_i\cap U_j$. We conclude that there exists an integer $p$ and
a section $s'_j$ of $\Ff$ over $U_j$ such that $s'_j=(t_i^p/t_j^p)\cdot s_i$
on $U_i\cap U_j$; for $j=i$, we have $s'_i = s_i$, which allows us to write
the preceding formula in the form $s'_j = (t_i^p/t_j^p)\cdot s'_i$.

The $s'_j$ being defined for every index $j$ (with the same exponent $p$),
consider $s'_j - (t_k^p/t_j^p)\cdot s'_k$; it is a section of $\Ff$ over
$U_j\cap U_k$ whose restriction to $U_i\cap U_j\cap U_k$ is zero;
by applying Proposition 6 of \no \ref{section-43} we see that for every sufficiently large
integer $q$ we have $(t_i^q/t_j^q)(s'_j - (t_k^p/t_j^p)\cdot s'_k) = 0$
on $U_j\cap U_k$; if we then put $s_j  = (t_i^q/t_j^q)\cdot s'_j$ and
$n = p+q$, the above formula is written $s_j = (t_k^n/t_j^n)\cdot s_k$
and the system $s = (s_j)$ is a section of $\Ff(n)$ whose $i$-th component
is equal to $s_i$, q.e.d.

{\bf Corollary.} \emph{Every coherent algebraic sheaf $\Ff$ on $X=\Pp_r(K)$
is isomorphic to a quotient sheaf of a sheaf $\Oo(n)^p$, $n$ and $p$
being suitable integers.}

By the above theorem, there exists an integer $n$ such that $\Ff(-n)_x$
is generated by $\Gamma(X, \Ff(-n))$ for every $x\in X$; by the
quasi-compactness of $X$, this is equivalent to saying that $\Ff(-n)$ is
isomorphic to a quotient sheaf of a sheaf $\Oo^p$, $p$ being an appropriate
integer $\geq 0$. It follows then that $\Ff\isom\Ff(-n)(n)$ is isomorphic
to a quotient sheaf of $\Oo(n)^p\isom \Oo^p(n)$.

\ssubsection{Graded modules}
\label{section-\arabic{subsection}}

Let $S = K[t_0, \ldots, t_r]$ be the algebra of polynomials in $t_0, \ldots, t_r$;
for every integer $n\geq 0$, let $S_n$ be the linear subspace of $S$ consisting
by homogeneous polynomials of degree $n$; for $n<0$, we set $S_n = 0$. The
algebra $S$ is a direct sum of $S_n$, $n\in\Zz$ and we have $S_p\cdot S_q
\subset S_{p+q}$; in other words, $S$ is a \emph{graded algebra}.

Recall that an $S$--module $M$ is said to be \emph{graded} if there is given
a decomposition of $M$ into a direct sum: $M = \bigoplus_{n\in\Zz} M_n$,
$M_n$ being subgroups of $M$ such that $S_p\cdot M_q\subset M_{p+q}$
for every couple $(p, q)$ of integers. An element of $M_n$ is said to be
\emph{homogeneous} of degree $n$; a submodule $N$ of $M$ is said to be
\emph{homogeneous} if it is a direct sum of $N\cap M_n$, in which case it
is a graded $S$--module. If $M$ and $M'$ are two graded $S$--modules, an
$S$--homomorphism 
\[ \phi: M\to M' \]
is said to be \emph{homogeneous of degree $s$} if $\phi(M_n)\subset M'_{n+s}$
for every $n\in\Zz$. A homogeneous $S$--homomorphism of degree 0 is simply
called a \emph{homomorphism}.

If $M$ is a graded $S$--module and $n$ an integer, we denote by $M(n)$ the
graded $S$--module:
\[ M(n) = \bigoplus_{p\in\Zz} M(n)_p \text{ with } M(n)_p = M_{n+p}. \]
%
% page 249
%
We thus have $M(n) = M$ as $S$--modules, but a homogeneous element of degree
$p$ of $M(n)$ is homogeneous of degree $n+p$ in $M$; in other words, $M(n)$
is made from $M$ by lowering degrees by $n$ units.

We denote by $\Ca$ the class of graded $S$--modules $M$ such that $M_n=0$
for $n$ sufficiently large. If $A\to B\to C$ is an exact sequence of
homomorphisms of graded $S$--modules, the relations $A\in\Ca$, $C\in \Ca$
clearly imply $B\in\Ca$; in other words, $\Ca$ is a class in the sense
of \cite{14}, Chap. I. Generally, we use the terminology introduced in the
aforementioned article; in particular, a homomorphism $\phi:A\to B$
is called \emph{$\Ca$-injective} (resp. \emph{$\Ca$-surjective}) if 
$\Ker(\phi)\in\Ca$ (resp. if $Coker(\phi)\in\Ca$) and $\Ca$-bijective
if it is both $\Ca$-injective and $\Ca$-surjective.

A graded $S$--module $M$ is said to be \emph{of finite type} if it is 
generated by a finite number of elements; we say that $M$ \emph{satisfies
the condition (TF)} if there exists an integer $p$ such that the submodule
$\bigoplus_{n\geq p} M_n$ of $M$ is of finite type; it is the same to say
that $M$ is $\Ca$-isomorphic to a module of finite type. The modules
satisfying (TF) form a class containing $\Ca$.

A graded $S$--module $L$ is called \emph{free} (resp. \emph{free of finite
type}) if it admits a base (resp. a finite base) consisting of homogeneous
elements, in other words if it is isomorphic to a direct sum (resp.
to a finite direct sum) of the modules $S(n_i)$.

\ssubsection{The algebraic sheaf associated to a graded $S$-module}
\label{section-\arabic{subsection}}

If $U$ is a nonempty subset of $X$, we denote by $S(U)$ the subset
of $S = K[t_0, \ldots, t_r]$ consisting of homogeneous polynomials $Q$
such that $Q(x)\neq 0$ for all $x\in U$; $S(U)$ is a multiplicatively closed
subset of $S$, not containing 0. For $U = X$, we write $S(x)$ instead of
$S(\{x\})$. 

Let $M$ be a graded $S$--module. We denote by $M_U$ the set of fractions
$m/Q$ with $m\in M$, $Q\in S(U)$, $m$ and $Q$ being homogeneous of 
\emph{the same} degree; we identify two fractions $m/Q$ and $m'/Q'$ if
there exists $Q''\in S(U)$ such that
\[ Q''(Q'\cdot m - Q\cdot m') = 0; \]
it is clear that we have defined an equivalence relation between the
pairs $(m, Q)$. For $U = x$, we write $M_x$ instead of $M_{\{x\}}$.

Applying this to $M = S$, we see that $S_U$ is the ring of rational
functions $P/Q$, $P$ and $Q$ being homogeneous polynomials of the same degree
and $Q\in S(U)$; if $M$ is an arbitrary graded $S$--module, we can equip
$M_U$ with a structure of an $S_U$--module by imposing:
\begin{align*}
	m/Q + m'/Q' &= (Q'm + Qm')/QQ' \\
	(P/Q)\cdot (m/Q') &= Pm/QQ'.
\end{align*}

If $U\subset V$, we have $S(V)\subset S(U)$, hence the canonical homomorphisms
\[ \phi_U^V : M_V\to M_U; \]
the system $(M_U, \phi_U^V)$, where $U$ and $V$ run over nonempty open
subsets of $X$, define thus a sheaf which we denote by $\Aa(M)$; we verify
immediately that
\[ \lim_{x\in U} M_U = M_x , \]
%
% page 250
%
that is, that $\Aa(M)_x = M_x$. In particular, we have $\Aa(S) = \Oo$ and
as the $M_U$ are $S_U$--modules, it follows that $\Aa(M)$ is a sheaf of
$\Aa(S)$--modules, that is, an \emph{algebraic sheaf} on $X$. Any 
homomorphism $\phi:M\to M'$ defines in a natural way the $S_U$-linear
homomorphisms $\phi_U:M_U\to M'_U$, thus a homomorphism of sheaves
$\Aa(\phi): \Aa(M)\to \Aa(M')$, which we frequently denote $\phi$.
We clearly have
\[ \Aa(\phi+\psi) = \Aa(\phi)+\Aa(\psi), \quad \Aa(1)=1,
	\quad \Aa(\phi\circ\psi) = \Aa(\phi)\circ\Aa(\psi). \]
The operation $\Aa(M)$ is thus a \emph{covariant additive functor}
defined on the category of graded $S$--modules and with values in
the category of algebraic sheaves on $X$.

(The above definitions are quite analogous to these of \S 4, from Chap. II;
it should be noted however that $S_U$ \emph{is not} the localization
of $S$ in $S(U)$, but only its homogeneous component of degree 0.)

\ssubsection{First properties of the functor $\Aa(M)$}
\label{section-\arabic{subsection}}

{\bf Proposition 3.} \emph{The functor $\Aa(M)$ is an exact functor.}

Let $M\xto{\alpha}M'\xto{\beta}M'"$ be an exact sequence of graded
$S$--modules and show that the sequence $M_x\xto{\alpha}M'_x\xto{\beta}
M''_x$ is also exact. Let $m'/Q\in M'_x$ be an element of the kernel
of $\beta$; by the definition of $M''_x$, there exist $R\in S(x)$
such that $R\beta(m')=0$; but then there exists $m\in M$ such that
$\alpha(m) = Rm'$ and we have $\alpha(m/RQ) = m'/Q$, q.e.d.\\
(Compare with \no \ref{section-48}, Lemma 1.)

{\bf Proposition 4.} \emph{If $M$ is a graded $S$--module and if $n$ is
an integer, $\Aa(M(n))$ is canonically isomorphic to $\Aa(M)(n)$.}

Let $i\in I$, $x\in U_i$ and $m/Q\in M(n)_x$, with $m\in M(n)_p$, $Q\in S(x)$,
$\deg Q = p$. Put:
\[ \eta_{i, x}(m/Q) = m/{t_i^n Q} \in M_x, \]
which is valid because $m\in M_{n+p}$ and $t_i^n Q\in S(x)$. We immediately
see that $\eta_{i, x} : M(n)_x\to M_x$ is bijective for all $x\in U_i$
and defines an isomorphism $\eta_i$ of $\Aa(M(n))$ to $\Aa(M)$ over
$U_i$. Moreover, we have $\eta_i\circ\eta_j^{-1}=\theta_{ij}(n)$
over $U_i\cap U_j$. By the definition of the operation $\Ff(n)$ and
Proposition 4 of \no \ref{section-4}, this shows that $\Aa(M(n))$ is isomorphic to
$\Aa(M)(n)$.

{\bf Corollary.} \emph{$\Aa(S(n))$ is canonically isomorphic to $\Oo(n)$.}

Indeed, it has been said that $\Aa(S)$ was isomorphic to $\Oo$.

(It is also clear that $\Aa(S(n))$ is isomorphic to $\Oo'(n)$, because
$\Oo'(n)_x$ consists precisely of the rational functions $P/Q$ such that
$\deg P - \deg Q = n$ and $Q\in S(x)$.)

{\bf Proposition 5.} \emph{Let $M$ be a graded $S$--module satisfying the
condition (TF). The algebraic sheaf $\Aa(M)$ is also a coherent sheaf.
Moreover $\Aa(M) = 0$ if and only if $M\in\Ca$.}

If $M\in\Ca$, for all $m\in M$ and $x\in X$ there exists $Q\in S(x)$ such
that $Qm = 0$; it suffices to take $Q$ of a sufficiently large degree;
we thus have $M_x = 0$, hence $\Aa(M) = 0$. Let now $M$ be a graded
$S$--module satisfying the condition (TF); there exists a
%
% page 251
%
homogeneous submodule $N$ of $M$, of finite type and such that $M/N\in\Ca$;
applying the above together with Proposition 3, we see that $\Aa(N)\to\Aa(M)$
is bijective and it thus suffices to prove that $\Aa(N)$ is coherent. 
Since $N$ is of finite type, there exists an exact sequence 
$L^1\to L^0\to N\to 0$ where $L^0$ and $L^1$ are \emph{free} modules of
finite type. By Proposition 3, the sequence $\Aa(L^1)\to\Aa(L^0)\to\Aa(N)\to 0$
is exact. But, by the corollary to Proposition 4, $\Aa(L^0)$ and $\Aa(L^1)$
are isomorphic to finite direct sums of the sheaves $\Oo(n_i)$ and are
thus coherent. It follows that $\Aa(N)$ is coherent.

Let finally $M$ be a graded $S$--module satisfying (TF) and such that
$\Aa(M) = 0$; by the above considerations, we can suppose that $M$ is
of finite type. If $m$ is a homogeneous element of $M$, let $\ai_m$
be the annihilator of $m$, that is, the set of all polynomials $Q\in S$
such that $Q\cdot m = 0$; it is clear that $\ai_m$ is a homogeneous
ideal. Moreover, the assumption $M_x=0$ for all $x\in X$ implies that
the variety of zeros of $\ai_m$ in $K^{r+1}$ is reduced to $\{0\}$;
Hilbert's theorem of zeros shows that every homogeneous polynomial
of sufficiently large degree belongs to $\ai_m$. Applying this to
the finite system of generators of $M$, we conclude immediately $M_p = 0$
for $p$ sufficiently large, which completes the proof.

By combining Propositions 3 and 5 we obtain:

{\bf Proposition 6.} \emph{Let $M$ and $M'$ be two graded $S$--modules 
satisfying the condition (TF) and let $\phi:M\to M'$ be a homomorphism
of $M$ to $M'$. Then 
\[ \Aa(\phi) : \Aa(M) \to \Aa(M') \]
is injective (resp. surjective, bijective) if and only if $\phi$ is 
$\Ca$-injective (resp. $\Ca$-surjective, $\Ca$-bijective).}

\ssubsection{The graded $S$--module associated to an algebraic sheaf}
\label{section-\arabic{subsection}}

Let $\Ff$ be an algebraic sheaf on $X$ and set:
\[ \Gamma(\Ff) = \bigoplus_{n\in\Zz}\Gamma(\Ff)_n,\quad
	\text{with}\quad \Gamma(\Ff)_n = \Gamma(X, \Ff(n)). \]

The group $\Gamma(\Ff)$ is a graded group; we shall equip it with a
structure of an $S$--module. Let $s\in\Gamma(X, \Ff(q))$ and let
$P\in S_p$; we can identify $P$ with a section of $\Oo(p)$
(cf. \no \ref{section-54}), thus $P\otimes s$ is a section of $\Oo(p)\otimes\Ff(q)
= \Ff(q)(p) = \Ff(p+q)$, using the homomorphisms from \no \ref{section-54}; we have
then defined a section of $\Ff(p+q)$ which we denote by $P\cdot s$
instead of $P\otimes s$. The mapping $(P, s)\to P\cdot s$ equips
$\Gamma(\Ff)$ with a structure of an $S$--module that is compatible
with the grading.

In order to compare the functors $\Aa(M)$ and $\Gamma(\Ff)$ we define two
canonical homomorphisms:
\[ \alpha:M\to\Gamma(\Aa(M))\quad\text{and}\quad\beta:\Aa(\Gamma(\Ff))\to\Ff.\]

% page 252

{\bf Definition of $\alpha$.} Let $M$ be a graded $S$--module and let $m\in M_0$
be a homogeneous element of $M$ of degree 0. The element $m/1$ is a well-defined
element of $M_x$ that varies continuously with $x\in X$; thus $m$ defines a
section $\alpha(m)$ of $\Aa(M)$. If now $m$ is homogeneous of degree $n$, $m$
is homogeneous of degree 0 in $M(n)$, thus defines a section $\alpha(m)$
of $\Aa(M(n)) = \Aa(M)(n)$ (cf. Proposition 4). This is the definition of
$\alpha:M\to\Gamma(\Aa(M))$ and it is immediate that it is a homomorphism.

{\bf Definition of $\beta$.} Let $\Ff$ be an algebraic sheaf on $X$ and let
$s/Q$ be an element of $\Gamma(\Ff)_x$ with $s\in\Gamma(X, \Ff(n))$, $Q\in S_n$
and $Q(x)\neq 0$. The function $1/Q$ is homogeneous of degree $-n$ and regular
in $x$, hence a section of $\Oo(-n)$ in a neighborhood of $x$; it follows
that $1/Q\otimes s$ is a section of $\Oo(-n)\otimes\Ff(n)=\Ff$ in a neighborhood
of $x$, thus defines an element of $\Ff_x$ which we denote by $\beta_x(s/Q)$,
because it depends only on $s/Q$. We can also define $\beta_x$ by using the
components $s_i$ of $s$: if $x\in U_i$, $\beta_x(s/Q) = (t_i^n/Q)\cdot s_i(x)$.
The collection of the homomorphisms $\beta_x$ defines a homomorphism
$\beta: \Aa(\Gamma(\Ff)) \to \Ff$.

The homomorphisms $\alpha$ and $\beta$ are related by the following Propositions,
which are shown by direct computation:

{\bf Proposition 7.} \emph{Let $M$ be a graded $S$--module. The composition of
the homomorphisms $\Aa(M)\to\Aa(\Gamma(\Aa(M)))\to\Aa(M)$ is the identity.}

(The first homomorphism is defined by $\alpha:M\to\Gamma(\Aa(M))$ and the second
is $\beta$, applied to $\Ff=\Aa(M)$.)

{\bf Proposition 8.} \emph{Let $\Ff$ be an algebraic sheaf on $X$. The composition
of the homomorphisms $\Gamma(\Ff)\to\Gamma(\Aa(\Gamma(\Ff)))\to\Gamma(\Ff)$
is the identity.}

(The first homomorphism is $\alpha$, applied to $M=\Gamma(\Ff)$, while the
second one is defined by $\beta:\Aa(\Gamma(\Ff))\to\Ff$.)

We will show in \no \ref{section-65} that $\beta:\Aa(\Gamma(\Ff))\to \Ff$ is bijective if 
$\Ff$ is coherent and that $\alpha:M\to\Gamma(\Aa(M))$ is $\Ca$-bijective
if $M$ satisfies the condition (TF).

\ssubsection{The case of coherent algebraic sheaves}
\label{section-\arabic{subsection}}

Let us show a preliminary result:

{\bf Proposition 9.} \emph{Let $\Ll$ be an algebraic sheaf on $X$, a direct
sum of a finite number of the sheaves $\Oo(n_i)$. Then $\Gamma(\Ff)$ 
satisfies (TF) and $\beta:\Aa(\Gamma(\Ll))\to \Ll$ is bijective.}

It comes down immediately $\Ll=\Oo(n)$, then to $\Ll = \Oo$. In this case,
we know that $\Gamma(\Oo(p)) = S_p$ for $p\geq 0$, thus we have $S\subset
\Gamma(\Oo)$, the quotient belonging to $\Ca$. It follows first that
$\Gamma(\Oo)$ satisfies (TF), then that $\Aa(\Gamma(\Oo))=\Aa(S)=\Oo$, q.e.d.

(We observe that we have $\Gamma(\Oo)=S$ if $r\geq 1$; on the other hand,
if $r = 0$, $\Gamma(\Oo)$ is not even an $S$--module of finite type.)

{\bf Theorem 2.} \emph{For every coherent algebraic sheaf $\Ff$ on $X$
there exists a graded $S$--module $M$, satisfying (TF), such that
$\Aa(M)$ is isomorphic to $\Ff$.}

By the corollary to Theorem 1, there exists an exact sequence of algebraic
sheaves:
\[ \Ll^1\xto{\phi}\Ll^0\to\Ff\to 0, \]
%
% page 253
%
where $\Ll^1$ and $\Ll^0$ satisfy the assumptions of the above Proposition.
Let $M$ be the cokernel of the homomorphism $\Gamma(\phi):\Gamma(\Ll^1)
\to\Gamma(\Ll^0)$; by Proposition 9, $M$ satisfies the condition (TF).
Applying the functor $\Aa$ to the exact sequence:
\[ \Gamma(\Ll^1)\to \Gamma(\Ll^0)\to M\to 0, \]
we obtain an exact sequence:
\[ \Aa(\Gamma(\Ll^1))\to \Aa(\Gamma(\Ll^0))\to\Aa(M)\to 0. \]


Consider the following commutative diagram:
\[
\begin{tikzpicture}[description/.style={fill=white,inner sep=2pt}]
    \matrix (m) [matrix of math nodes, row sep=3em,
    column sep=2.5em, text height=1.5ex, text depth=0.25ex]
    {   \Aa(\Gamma(\Ll^1)) & \Aa(\Gamma(\Ll^0)) & \Aa(M) & 0 \\
        \Ll^1 & \Ll^0 & \Ff & 0 \\ };
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ $} (m-1-2)
    (m-1-2) edge node[auto] {$ $} (m-1-3)
    (m-1-3) edge node[auto] {$ $} (m-1-4);
    \path[->,font=\scriptsize]
    (m-2-1) edge node[auto] {$ $} (m-2-2)
    (m-2-2) edge node[auto] {$ $} (m-2-3)
    (m-2-3) edge node[auto] {$ $} (m-2-4);
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ \beta $} (m-2-1);
    \path[->,font=\scriptsize]
    (m-1-2) edge node[auto] {$ \beta $} (m-2-2);
\end{tikzpicture}
\]

By Proposition 9, the two vertical homomorphisms are bijective. It follows
that $\Aa(M)$ is isomorphic to $\Ff$, q.e.d.

